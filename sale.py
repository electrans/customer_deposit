# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, Bool, Not
from trytond.i18n import gettext
from trytond.exceptions import UserError

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    to_custody = fields.Boolean(
        "To custody",
        states={'readonly': Eval('state') != 'draft'},
        depends=['state'])
    internal_shipments = fields.Function(
        fields.One2Many(
            'stock.shipment.internal', None, "Internal Shipments",
            states={'invisible': Not(Eval('to_custody', False))}),
        'get_internal_shipments')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.shipment_method.depends.append('to_custody')
        cls.shipment_method.domain = [
            If(Bool(Eval('to_custody')),
                ('shipment_method', '=', 'manual'), ())]

    @classmethod
    def process(cls, sales):
        done = []
        process = []
        for sale in sales:
            if sale.to_custody:
                if sale.state not in ('confirmed', 'processing', 'done'):
                    continue
                sale.create_invoice()
                sale.set_invoice_state()
                sale.create_shipment('internal')
                sale.set_shipment_state()
                if sale.is_done():
                    if sale.state != 'done':
                        if sale.state == 'confirmed':
                            process.append(sale)
                        cls.do(done)
                elif sale.state != 'processing':
                    process.append(sale)
            if process:
                cls.proceed(process)
            else:
                super(Sale, cls).process([sale])

    def create_shipment(self, shipment_type):
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        CompanyBranch = pool.get('company.branch')
        Location =pool.get('stock.location')
        shipments = []
        branch = []
        if shipment_type != 'internal':
            shipments = super(Sale, self).create_shipment(shipment_type)
        else:
            # search for the shipment_party warehouse and if not use the sale warehouse
            if self.shipment_party:
                branch = CompanyBranch.search([('party', '=', self.shipment_party.id)])
                warehouse = branch[0].warehouse if self.shipment_party and branch else None
            else:
                warehouse = self.warehouse
            if warehouse:
                to_location = None
                from_location = self.warehouse.storage_location.id if self.warehouse.output_location else None
                # search for the customer deposit location for the sale customer inside the warehouse
                locations = Location.search([('customer_deposit', '=', True), ('customer', '=', self.party.id)])
                for location in locations:
                    if location.warehouse.id == self.warehouse.id:
                        to_location = location.id
                if from_location and to_location:
                    # create an internal shipment and the moves.
                    shipment, = ShipmentInternal.create([{
                        'from_location': from_location,
                        'to_location': to_location
                    }])
                    moves = []
                    for line in self.lines:
                        move = line.get_move(shipment_type)
                        if move:
                            move.save()
                            move.from_location = from_location
                            move.to_location = to_location
                            move.shipment = str(shipment)
                            move.save()
                            moves.append(move)
                    if not moves:
                        ShipmentInternal.delete([shipment])
                        return
                else:
                    raise UserError(gettext('electrans_customer_deposit.not_deposit_location', warehouse=warehouse.rec_name))
                shipments = [shipment]
                ShipmentInternal.wait(shipments)
        return shipments

    def _get_shipped_quantity(self, shipment_type):
        # if the shipment_type is internal, just return the shipped quantity of the internal moves.
        pool = Pool()
        Uom = pool.get('product.uom')
        ShipmentInternal = pool.get('stock.shipment.internal')
        if shipment_type == 'internal':
            quantity = 0
            skips = set(m for m in self.moves_recreated)
            for move in self.moves:
                if move not in skips and move.shipment and isinstance(move.shipment, ShipmentInternal):
                    quantity += Uom.compute_qty(move.uom, move.quantity,
                        self.unit)
        else:
            quantity = super(Sale, self)._get_shipped_quantity(shipment_type)
        return quantity

    def get_internal_shipments(self, name=None):
        ShipmentInternal = Pool().get('stock.shipment.internal')
        shipments = set()
        for line in self.lines:
            for move in line.moves:
                if isinstance(move.shipment, ShipmentInternal):
                    shipments.add(move.shipment.id)
        return list(shipments)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_move(self, shipment_type):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        if shipment_type == 'internal':
            if self.type != 'line':
                return
            if not self.product:
                return
            if self.product.type == 'service':
                return

            quantity = (abs(self.quantity) - self._get_shipped_quantity(shipment_type))

            quantity = self.unit.round(quantity)
            if quantity <= 0:
                return

            move = Move()
            move.quantity = quantity
            move.uom = self.unit
            move.product = self.product
            move.from_location = self.from_location
            move.to_location = self.to_location
            move.state = 'draft'
            move.company = self.sale.company
            move.unit_price = self.unit_price
            move.currency = self.sale.currency
            if self.moves:
                # backorder can not be planned
                move.planned_date = Date.today()
            else:
                move.planned_date = self.shipping_date
            move.invoice_lines = self._get_move_invoice_lines(shipment_type)
            move.origin = self
            move.description = move.get_description()
        else:
            move = super(SaleLine, self).get_move(shipment_type)
        return move
