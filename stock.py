# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, Not


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    customer_deposit = fields.Boolean(
        "Customer Deposit",
        states={'invisible': Not(Eval('type') == 'storage')},
        depends=['type'])
    customer = fields.Many2One(
        'party.party', "Customer",
        states={'invisible': Not(Eval('customer_deposit', False))},
        depends=['customer_deposit'])


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def _set_transit(cls, shipments):
        # Remove the origin of the incoming moves if it is a sale
        pool = Pool()
        SaleLine = pool.get('sale.line')
        super(ShipmentInternal, cls)._set_transit(shipments)
        for shipment in shipments:
            if not shipment.transit_location:
                continue
            for move in shipment.incoming_moves:
                if isinstance(move.origin, SaleLine):
                    move.origin = None
                    move.save()
